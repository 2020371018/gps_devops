import './App.css';
import { useEffect, useState } from 'react';
import axios from 'axios';

function App() {
  const [notes, setNotes] = useState([]);
  const [newNote, setNewNote] = useState({ title: '', text: '' });

  useEffect(() => {
    axios.get('http://localhost:3000/api/note')
      .then((response) => setNotes(response.data))
      .catch((error) => console.error('Error fetching notes:', error));
  }, []);

  const handleNoteSubmit = () => {
    // Verificar que el título no esté vacío antes de crear una nota
    if (newNote.title.trim() === '') {
      alert('El título no puede estar vacío.');
      return;
    }
    // Verificar que la nota no esté vacía
    if (newNote.text.trim() === '') {
      alert('La nota no puede estar vacía.');
      return;
    }

    axios.post('http://localhost:3000/api/note', newNote)
      .then((response) => {
        setNotes([...notes, response.data]);
        setNewNote({ title: '', text: '' });
      })
      .catch((error) => console.error('Error creating note:', error));
  };

  return (
    <div className="App">
      <header className="App-header">
        <form>
          <input
            type="text"
            placeholder="Título"
            value={newNote.title}
            onChange={(e) => setNewNote({ ...newNote, title: e.target.value })}
          />
          <input
            type="text"
            placeholder="Texto de la nota"
            value={newNote.text}
            onChange={(e) => setNewNote({ ...newNote, text: e.target.value })}
          />
          <button type="button" onClick={handleNoteSubmit}>
            Crear nota
          </button>
        </form>
        {notes.length === 0 ? (
          <p>No hay notas disponibles.</p>
        ) : (
          <ul>
            {notes.map((note) => (
              <li key={note._id}>
                <h3>{note.title}</h3>
                <p>{note.text}</p>
              </li>
            ))}
          </ul>
        )}
      </header>
    </div>
  );
}

export default App;
