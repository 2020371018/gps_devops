// Importar el archivo de configuracion
importScripts('js/sw-utils.js')

// Crear las variables de cache
const CACHE_DYNAMIC = 'dynamic-v1' // Para los archivos que se van a descargar
const CACHE_STATIC = 'static-v2' // App shell
const CACHE_INMUTABLE = 'inmutable-v1' // CDN de terceros, LIBRERIAS

const limpiarCache = (cacheName, numberItem) => {
    caches.open(cacheName)
        .then(cache => {
            cache.keys()
                .then(keys => {
                    if (keys.length > numberItem) {
                        cache.delete(keys[0])
                            .then(limpiarCache(cacheName, numberItem))
                    }
                })
        })
}

self.addEventListener('install', event => {
    const cachePromise = caches.open(CACHE_STATIC).then(function (cache) {
        return cache.addAll([
            '/',
            '/index.html',
            '/js/app.js',
            '/js/sw-utils.js',
            '/sw.js',
            '/favicon.ico',
            'static/js/bundle.js',
            'not-found.png',
            'pages/offline.html'
        ])
    })
    const cacheInmutable = caches.open(CACHE_INMUTABLE).then(function (cache) {
        return cache.addAll([
            'https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css'
        ])
    })
    event.waitUntil(Promise.all([cachePromise, cacheInmutable]))
})

// Función para eliminar cache anterior
self.addEventListener('activate', function (event) {
    const respuesta = caches.keys()
        .then(keys => {
            keys.forEach(key => {
                if (key !== CACHE_STATIC && key.includes('static')) {
                    return caches.delete(key)
                }
            })
        })
    event.waitUntil(respuesta)
})

self.addEventListener('fetch', function (event) {
    const respuesta = caches.match(event.request)
        .then(res => {
            if (res) {
                return res 
            } else {
                return fetch(event.request)
                    .then(newRes => {
                        return actualizarCacheDinamico(CACHE_DYNAMIC, event.request, newRes)
                    })
            }
        })
    event.respondWith(respuesta)
})